package ru.baulina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractAuthCommand {

    @NotNull
    @Override
    public String name() {
        return "unlocked";
    }

    @NotNull
    @Override
    public String description() {
        return "Unlocked user.";
    }

    @Override
    public void execute() {
        System.out.println("[LOCKED_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserLogin(login);
        System.out.println("[OK]");
        System.out.println();
    }

}
