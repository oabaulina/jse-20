package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX");
        @Nullable final Integer index = TerminalUtil.nexInt() -1;
        @NotNull final Long userId = serviceLocator.getAuthService().getUserId();
        @Nullable final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
