package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.entity.Task;

import java.util.List;

public final class TaskShowCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        @NotNull final Long userId = serviceLocator.getAuthService().getUserId();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        int index = 1;
        for (Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
