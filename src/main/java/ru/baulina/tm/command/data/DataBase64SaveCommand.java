package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataSaveException;

import java.io.*;
import java.nio.file.Files;
import java.util.Base64;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        @NotNull final Domain domain = new Domain();
        domainService.export(domain);
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
                @NotNull final ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
                @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream)
        ) {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            objectOutputStream.writeObject(domain);
            @NotNull final byte[] bytes = byteOutputStream.toByteArray();
            @NotNull final String Base64Encoded = Base64.getEncoder().encodeToString(bytes);
            @NotNull final byte[] bytesEncoded = Base64Encoded.getBytes();

            fileOutputStream.write(bytesEncoded);
            fileOutputStream.flush();
        } catch (IOException e) {
            throw new DataSaveException(e);
        }

        System.out.println("[OK]");
        System.out.println();
    }

}
