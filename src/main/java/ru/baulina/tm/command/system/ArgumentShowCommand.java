package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.command.AbstractCommand;

import java.util.Arrays;
import java.util.List;

public final class ArgumentShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        @NotNull final List<String> arguments = serviceLocator.getCommandService().getArgs();
        System.out.println(Arrays.toString(arguments.toArray()));
        System.out.println();
    }

}
