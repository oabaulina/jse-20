package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import java.util.List;

public interface IAuthService {

    @NotNull
    Long getUserId();

    void checkRole(@Nullable final Role[] roles);

    void isAuth();

    void logout();

    void login(@Nullable final String login, @Nullable final String password);

    void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email);

    void changePassword(@Nullable final String passwordOld, @Nullable final String passwordNew);

    @NotNull
    User findByLogin(@Nullable final String login);

    @Nullable
    User findById();

    @Nullable
    List<User> findAll();

    void changeUser(@Nullable final String email, @Nullable final String festName, @Nullable final String LastName);

}
