package ru.baulina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand command);

    void remove(@NotNull AbstractCommand command);

    @NotNull
    List<AbstractCommand> findAll();

    @NotNull
    List<String> getCommandsNames();

    @NotNull
    List<String> getArgs();

    void clear();

    @Nullable
    AbstractCommand getByArg(@NotNull String arg);

    @Nullable
    AbstractCommand getByName(@NotNull String name);

    @Nullable
    AbstractCommand removeByName(@NotNull String name);

    @Nullable
    AbstractCommand removeByArg(@NotNull String name);

}
