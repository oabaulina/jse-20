package ru.baulina.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.*;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.system.EmptyArgumentException;
import ru.baulina.tm.exception.system.UnknownCommandException;
import ru.baulina.tm.repository.CommandRepository;
import ru.baulina.tm.repository.ProjectRepository;
import ru.baulina.tm.repository.TaskRepository;
import ru.baulina.tm.repository.UserRepository;
import ru.baulina.tm.service.*;
import ru.baulina.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(userService, projectService, taskService);

    @SneakyThrows
    private void init() {
        @NotNull final Reflections reflections = new Reflections("ru.baulina.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.baulina.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void initUsers()  {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String[] args) {
        init();
        System.out.println("** Welcome to task manager **");
        System.out.println();
        try {
            if (parseArgs(args)) System.exit(0);
        } catch (Exception e) {
            logError(e);
            System.exit(0);
        }
        initUsers();
        process();
    }

    private void process() {
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private static void logError(Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        return parseArg(arg);
    }

    private boolean parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        @Nullable final AbstractCommand command = commandService.getByArg(arg);
        if (command == null) throw new UnknownCommandException(arg);
        command.execute();
        return true;
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

}
