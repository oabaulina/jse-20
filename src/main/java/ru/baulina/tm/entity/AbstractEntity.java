package ru.baulina.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Random;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEntity implements Serializable {

    private long id = Math.abs(new Random().nextLong());

}
